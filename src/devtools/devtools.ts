/**
 * Adds Puppeteer IDE panel in devtools window.
 */
function addIDEPanel() {
  chrome.devtools.panels.create(
    'ZMDSpider IDE',
    'devtools/idePanel/pptr.png',
    'devtools/idePanel/idePanel.html',
    () => {}
  );
}

addIDEPanel();
